# Flask skeleton

## Getting started

It is a very basic skeleton project of flask framework.

## Consists of

- [ ] [.flak8] handling irritating error message
- [ ] [docker-command.sh] handling docker images and containers:

```
$ ./docker-command.sh help
$ ./docker-command.sh build
$ ./docker-command.sh rebuild
$ ./docker-command.sh restart
$ ./docker-command.sh delete
```

## Author
Hwayoung Lee [mysonhs@gmail.com]

## Support
No support at all. It is on your lisk.

## License
Free to use.

