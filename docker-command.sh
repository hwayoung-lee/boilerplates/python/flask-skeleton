#!/bin/bash

command=$1

case $command in
    help)
        echo ""
        echo "usage: ./docker-command.sh [delete|build|restart|rebuild]"
        echo ""
        echo " - delete: delete all running containers"
        echo " - build: 'docker compose up -d'"
        echo " - restart: stop all running containers and start them again"
        echo " - rebuild: stop, remove and build containers again"
        exit 0
        ;;
    delete|restart|rebuild)
        echo $command

        var_running_containers=$(docker ps -q)
        echo $var_running_containers 
        ;;
esac

case $command in
    build)
        docker compose up -d
        ;;
    delete)
        docker stop $var_running_containers -t 1 
        docker rm $var_running_containers
        docker image rm $(docker images -q)
        ;;
    restart)
        docker stop $var_running_containers -t 1 
        docker start $var_running_containers
        ;;
    rebuild)
        docker stop $var_running_containers -t 1 
        docker rm $var_running_containers
        docker image rm $(docker images -q)
        docker compose up -d
        ;;
    * )
        echo "Unsupported option"
        echo "usage: ./docker-command.sh help "
        exit 0
        ;;
esac
