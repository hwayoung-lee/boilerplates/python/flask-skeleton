import traceback
from flask import jsonify


def exceptionMessage(e: Exception):
    traces = traceback.extract_tb(e.__traceback__)
    stackTrace = list()

    for trace in traces:
        stackTrace.append(
            "File : %s , Line : %d, Func.Name : %s, Message : %s" % (trace[0], trace[1], trace[2], trace[3])
        )

    response = jsonify({
        "type": e.__class__.__name__,
        "message": str(e),
        "trace": stackTrace
    })
    response.status_code = 500
    return response


def pre(s: str) -> str:
    return "<pre>" + s + "</pre>"
