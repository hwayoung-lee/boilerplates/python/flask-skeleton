def escaping():
    return "Hell'o' \"w\"orld!"


def newline():
    return "Hello \nworld!"


def docString():
    return '''
H
e
        l
    l
    o

        w
            o
        r
        l
    d
!
'''


def getLength(s: str) -> int:
    return len(s)


def getChar(s: str, pos: int) -> str:
    return s[pos]


def subString(s: str, pos1: int, pos2: int) -> str:
    return s[pos1:pos2]


def stringFormatPostion(message: str, name: str) -> str:
    return message.format(name)


def stringFormatPlaceHolder(message: str, name: str) -> str:
    return message.format(name=name)
