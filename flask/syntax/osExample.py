import os


def osPathCurrentDir():
    return os.path.dirname(os.path.abspath(__file__))


def rootDirectory():
    return os.path.abspath(os.curdir)


def readFile(fileName: str) -> str:
    if os.path.exists('./syntax/' + fileName):
        textBlock = open('./syntax/' + fileName).read()
    else:
        textBlock = "File not found."

    return textBlock


def fileList(directoryName: str) -> list:
    dirPath = './' + directoryName
    if os.path.exists(dirPath):
        fileList = [f for f in os.listdir(dirPath) if os.path.isfile(dirPath + '/' + f)]
    else:
        raise Exception("No directory exists.")

    return fileList
