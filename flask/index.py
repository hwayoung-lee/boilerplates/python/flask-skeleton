from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route('/')
def index():
    return "Hi!"


@app.route('/plus', methods=['GET'])
def getPlus():
    try:
        a = int(request.args.get('a'))
        b = int(request.args.get('b'))
        import syntax.number as number

        arr = [a, b, number.plus(a, b), str(number.plus(a, b))]

        response = jsonify({"data": arr})
        response.status_code = 200

        return response
    except Exception as e:
        import common.utils as utils
        return utils.exceptionMessage(e)
    # finally:


@app.route('/string/escaping', methods=['GET'])
def getStringEscaping():
    import syntax.stringExample as strExample
    return strExample.escaping()


@app.route('/string/newline', methods=['GET'])
def getStringNewline():
    import syntax.stringExample as strExample
    import common.utils as utils
    return utils.pre(strExample.newline())


@app.route('/string/docstring', methods=['GET'])
def getStringDocString():
    import syntax.stringExample as strExample
    import common.utils as utils
    return utils.pre(strExample.docString())


@app.route('/string/length', methods=['GET'])
def getLength():
    s = request.args.get('s')
    import syntax.stringExample as strExample
    return str(strExample.getLength(s))


@app.route('/string/char', methods=['GET'])
def getChar():
    try:
        s = request.args.get('s')
        pos = int(request.args.get('pos'))
        import syntax.stringExample as strExample
        return strExample.getChar(s, pos)
    except Exception as e:
        import common.utils as utils
        return utils.exceptionMessage(e)


@app.route('/string/substring', methods=['GET'])
def getSubString():
    try:
        s = request.args.get('s')
        pos1 = int(request.args.get('pos1'))
        pos2 = int(request.args.get('pos2'))

        import syntax.stringExample as strExample
        return strExample.subString(s, pos1, pos2)
    except Exception as e:
        import common.utils as utils
        return utils.exceptionMessage(e)


# /string/stringformatposition?message=Hello%20{}&name=Hwayoung
@app.route('/string/stringformatposition', methods=['GET'])
def stringFormatPosition():
    message = request.args.get('message')
    name = request.args.get('name')
    import syntax.stringExample as strExample
    return strExample.stringFormatPostion(message, name)


# /string/stringformatplaceholder?message=Hello%20{name}&name=Hwayoung
@app.route('/string/stringformatplaceholder', methods=['GET'])
def stringFormatPlaceHolder():
    message = request.args.get('message')
    name = request.args.get('name')
    import syntax.stringExample as strExample
    return strExample.stringFormatPlaceHolder(message, name)


@app.route('/os/curdir', methods=['GET'])
def getCurrentPath():
    import syntax.osExample as osExample
    return osExample.osPathCurrentDir()


@app.route('/os/rootdirectory', methods=['GET'])
def getRootDirectory():
    import syntax.osExample as osExample
    return osExample.rootDirectory()


@app.route('/os/readfile', methods=['GET'])
def readFile():
    fileName = request.args.get('filename')
    import syntax.osExample as osExample
    import common.utils as utils
    return utils.pre(osExample.readFile(fileName))


@app.route('/os/fileList', methods=['GET'])
def fileList():
    try:
        directoryName = request.args.get('dir')
        import syntax.osExample as osExample
        return osExample.fileList(directoryName)
    except Exception as e:
        import common.utils as utils
        return utils.exceptionMessage(e)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
